//
//  UIViewController+Alert.swift
//  QuizGame
//
//  Created by Александр Астапенко on 18.02.22.
//

import UIKit

extension UIViewController {
    func showAlert(alertController: UIAlertController,
                   actionButtonYes: UIAlertAction,
                   actionButtonNo: UIAlertAction)
                    {
        
        let alertController = alertController
        let yesAction = actionButtonYes
        let noAction = actionButtonNo
                        
        alertController.addAction(yesAction)
        alertController.addAction(noAction)
        present(alertController, animated: true, completion: nil)
    }
}

